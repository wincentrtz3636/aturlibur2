<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/plan.css')}}">
</head>
<body>
    <div class="landing-background" style="background: url('../public/images/kesuma.jpg');background-repeat: no-repeat;background-size: cover;background-position: center">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <strong><a class="navbar-brand" href="#">Atur Libur</a></strong>
                </div>
        
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{url('')}}">Home</a></li>
                        <li><a href="{{url('')}}">Cities</a></li>
                        <li><a href="{{url('')}}">Trips</a></li>
                        <li><a href="{{url('')}}">Contact Us</a></li>
                        @guest
                        <li><a href="{{url('/login')}}">Login</a></li> 
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{url('/list')}}">Holiday List</a></li>
                                <li><a href="{{url('/place')}}">Wishlist</a></li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
        <div style="padding-top: 250px;"></div>
        <div class="headline container-fluid">
            <h1>Dummy Trip</h1>
            <h4>14 December 2018 - 25 December 2018</h4>
        </div>
    </div>
    <div class="panel">
        <div class="rundown col-xs-8 col-sm-8 col-md-8 col-lg-8 container-fluid">
            <div class="container" style="margin-top:30px">
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <select name="" id="input" class="form-control" required="required">
                       
                        <option value="">14 December 2018</option>
                        <option value="">15 December 2018</option>
                        <option value="">16 December 2018</option>
                        <option value="">17 December 2018</option>
                        <option value="">18 December 2018</option>
                        <option value="">19 December 2018</option>
                        <option value="">20 December 2018</option>
                        <option value="">21 December 2018</option>
                        <option value="">22 December 2018</option>
                        <option value="">23 December 2018</option>
                        <option value="">24 December 2018</option>
                        <option value="">25 December 2018</option>

                        
                    </select>
                </div>
                
                
            </div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="text-center">Start time</th>
                        <th class="text-center">End time</th>
                        <th class="text-center">Cost</th>
                        <th class="text-center">Activity</th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="row1">
                        <td id="start1">06.00</td>
                        <td id="end1">08.00</td>
                        <td id="price1">Rp. 0 </td>
                        <td id="description1">Hotel Hard Rock Bali</td>       
                    </tr>
                    <tr id="row1T">
                        <td id="start1T">08.00</td>
                        <td id="end1T">08.42</td>
                        <td id="price1T">Rp. 20.000 - 30.000 </td>
                        <td id="description1T">Travelling</td>       
                    </tr>
                    <tr id="row2">
                        <td id="start1">08.42</td>
                        <td id="end1">12.00</td>
                        <td id="price1">Rp. 200.000 - 300.000</td>
                        <td id="description1">Nusa Dua</td>       
                    </tr>
                    <tr id="row2T">
                        <td id="start2T">12.00</td>
                        <td id="end2T">12.35</td>
                        <td id="price2T">Rp. 16.000 - 28.000 </td>
                        <td id="description2T">Travelling</td>       
                    </tr>
                    <tr id="row3">
                        <td id="start1">12.35</td>
                        <td id="end1">14.00</td>
                        <td id="price1">Rp. 100.000 - 200.000</td>
                        <td id="description1">Nook</td>       
                    </tr>
                    <tr id="row3T">
                        <td id="start3T">14.00</td>
                        <td id="end3T">14.14</td>
                        <td id="price3T">Rp. 10.000 - 20.000 </td>
                        <td id="description3T">Travelling</td>       
                    </tr>
                    <tr id="row4">
                        <td id="start1">14.14</td>
                        <td id="end1">16.00</td>
                        <td id="price1">Rp. 50.000 - 100.000</td>
                        <td id="description1">Dream Zone Museum Bali</td>       
                    </tr>
                    <tr id="row4T">
                        <td id="start3T">16.00</td>
                        <td id="end3T">16.15</td>
                        <td id="price3T">Rp. 10.000 - 20.000 </td>
                        <td id="description3T">Travelling</td>       
                    </tr>
                    <tr id="row4">
                        <td id="start1">16.15</td>
                        <td id="end1">18.30</td>
                        <td id="price1">Rp. 100.000 - 200.000</td>
                        <td id="description1">Pantai Kuta</td>       
                    </tr>
                    <tr id="row4T">
                        <td id="start3T">18.30</td>
                        <td id="end3T">18.37</td>
                        <td id="price3T">Rp. 4000 - 7000 </td>
                        <td id="description3T">Travelling</td>       
                    </tr>
                    <tr id="row5">
                        <td id="start1">18.37</td>
                        <td id="end1">20.00</td>
                        <td id="price1">Rp. 100.000 - Rp. 200.000</td>
                        <td id="description1">Hotel Hard Rock Bali</td>       
                    </tr>

                </tbody>
            </table>
        </div>
        <div class="wishlist col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <h2 style="color: #fca000">Wishlist</h2>
            <hr style="border-color: #fca000">
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVb6byfNVvArVNCh7jeUmj_oDjQgNHd8c"></script>
</body>
</html>