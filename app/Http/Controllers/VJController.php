<?php

namespace App\Http\Controllers;

use App\VJ;
use Illuminate\Http\Request;

class VJController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VJ  $vJ
     * @return \Illuminate\Http\Response
     */
    public function show(VJ $vJ)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VJ  $vJ
     * @return \Illuminate\Http\Response
     */
    public function edit(VJ $vJ)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VJ  $vJ
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VJ $vJ)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VJ  $vJ
     * @return \Illuminate\Http\Response
     */
    public function destroy(VJ $vJ)
    {
        //
    }
}
